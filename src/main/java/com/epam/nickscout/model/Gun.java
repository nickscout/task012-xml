package com.epam.nickscout.model;

import lombok.Data;

@Data
public class Gun {
    private String name;
    private double caliber;
    private double weight;
    private double muzzleVelocity;
    private double effectiveRange;
    private Barrel barrel;
    private GunType gunType;
    private Action action;
    private String origin;

}

enum Barrel {
    threaded,
    smoothbore
}

enum GunType {
    handgun,
    shotgun,
    smg,
    assaultRifle,
    rifle
}

enum Action {
    bolt (0),
    semiAuto (1),
    fullAuto (2),
    cutoff (3);

    private final int Value;

    Action(int value) {
        Value = value;
    }

    public int getValue() {
        return Value;
    }
}